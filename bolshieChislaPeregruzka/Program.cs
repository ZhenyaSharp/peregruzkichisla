﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bolshieChislaPeregruzka
{
    class Program
    {
        static int SizeOfArray()
        {
            Console.WriteLine("Input size of number");
            int size = int.Parse(Console.ReadLine());
            return size;
        }

        static void Main(string[] args)
        {
            Number num1 = new Number(SizeOfArray());
            num1.FillArray();

            Number num2 = new Number(SizeOfArray());
            num2.FillArray();

            num1.Add(num2);

            Console.WriteLine(num1);

            Console.ReadKey();

        }
    }
}
