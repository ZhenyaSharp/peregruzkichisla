﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace bolshieChislaPeregruzka
{
    class Number
    {
        private int[] number;

        public Number(int size)
        {
            number = new int[size];
            ClearArray();
        }

        public void ClearArray()
        {
            for (int i = 0; i < number.Length; i++)
            {
                number[i] = 0;
            }
        }

        public Number(Number num)
        {
            this.number = new int[num.number.Length];

            for (int i = 0; i < num.number.Length; i++)
            {
                this.number[i] = num.number[i];
            }
        }

        public void FillArray()
        {
            for (int i = 0; i < number.Length; i++)
            {
                Console.WriteLine($"Input {i + 1} digit");
                number[i] = int.Parse(Console.ReadLine());
            }
        }

        public int Length
        {
            get
            {
                return number.Length;
            }
        }

        public void CheckAndResizeArray(Number num)
        {
            int diff = 0;

            if (this.Length > num.Length) // если 1 массив больше 2
            {
                diff = this.Length - num.Length; // то находим разницу

                int[] newNumber = new int[this.Length]; // чтобы уравнять их, создаем новый массив с размером большего

                int newI = 0;

                for (int i = 0; i < newNumber.Length; i++) // заполняем новый массив
                {
                    if (i >= diff) // недостающие элементы заполняются нулями
                    {
                        newNumber[i] = num.number[newI]; // остальные переносим из меньшего
                        newI++;
                    }
                }
                num.number = newNumber; // перекидываем ссылку
            }

            if (this.Length < num.Length) // и наоборот если 2 больше 1
            {
                diff = num.Length - this.Length;

                int[] newNumber = new int[num.Length];

                int newI = 0;

                for (int i = 0; i < newNumber.Length; i++)
                {
                    if (i >= diff)
                    {
                        newNumber[i] = number[newI];
                        newI++;
                    }
                }
                number = newNumber;
            }
        }

        public void Add(Number num)
        {
            CheckAndResizeArray(num);

            for (int i = number.Length-1; i >= 0; i--)
            {
                this.number[i] += num.number[i];

                if (number[i] >= 10)
                {
                    number[i] %= 10;
                    number[i - 1] += 1;
                }
            }
        }


        public override string ToString()
        {
            string output = "";

            for (int i = 0; i < number.Length; i++)
            {
                output += number[i];
            }

            return output;
        }

        public static Number operator +(Number num1, Number num2)
        {
            Number mResult = new Number(num1);
            mResult.Add(num2);
            return mResult;
        }


    }
}
